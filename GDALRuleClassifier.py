
# Import required libraries from python
import sys, os, struct
# Import gdal
import osgeo.gdal as gdal

# Define the GDALRuleClassifier class
class GDALRuleClassifier (object):

    # A function to create the output image with a given number of image bands.
    def createOutputImage(self, outFilename, inDataset, numOutBands):
        # Define the image driver to be used 
        # This defines the output file format (e.g., GeoTiff)
        driver = gdal.GetDriverByName( "GTiff" )
        # Check that this driver can create a new file.
        metadata = driver.GetMetadata()

        # Get the spatial information from the input file
        geoTransform = inDataset.GetGeoTransform()
        geoProjection = inDataset.GetProjection()
        # Create an output file of the same size as the inputted 
        # image but with numOutBands output image bands.
        newDataset = driver.Create(outFilename, inDataset.RasterXSize, \
                     inDataset.RasterYSize, numOutBands, gdal.GDT_Float32)
        # Define the spatial information for the new image.
        newDataset.SetGeoTransform(geoTransform)
        newDataset.SetProjection(geoProjection)
        return newDataset

    # The function which runs the classification.
    def classifyImage(self, filePath, outFilePathQKL, outFilePathSpatial):
        # Open the inputted dataset
        dataset = gdal.Open( filePath, gdal.GA_ReadOnly )
        # Check the dataset was successfully opened
        if dataset is None:
            print ("The dataset could not openned")
            sys.exit(-1)
    
        # Create the output dataset (Coloured Image)
        outDatasetQKL = self.createOutputImage(outFilePathQKL, dataset, 3)
        # Check the datasets was successfully created.
        if outDatasetQKL is None:
            print ('Could not create quicklook output image')
            sys.exit(-1)
        
        # Create the output dataset (Single band Image)
        outDataset = self.createOutputImage(outFilePathSpatial, dataset, 1)
        # Check the datasets was successfully created.
        if outDataset is None:
            print ('Could not create output image')
            sys.exit(-1)
        
        # Open the NDVI image band
        ndvi_band = dataset.GetRasterBand(1) # NDVI BAND
        numLines = ndvi_band.YSize
        # Define variables for pixel output
        outClass = 0
        red = 0
        green= 0
        blue = 0
        # Loop through the image lines
        for line in range(numLines):
            # outputLine = ''
            # outputLineR = ''
            # outputLineG = ''
            # outputLineB = ''
            # Read in data for the current line from the 
            # image band representing the NDVI
            ndvi_scanline = ndvi_band.ReadRaster( 0, line, ndvi_band.XSize, 1, \
                                        ndvi_band.XSize, 1, gdal.GDT_Float32 )
            # Unpack the line of data to be read as floating point data
            ndvi_tuple = struct.unpack('f' * ndvi_band.XSize, ndvi_scanline)
            print(line)
            # Loop through the row and assess each pixel.
            for i in range(len(ndvi_tuple)):
                # If statements are used to encode the rules.
                if ndvi_tuple[i] < 0:
                    outClass = 0 # Output class
                    red = 200 # Quantity of Red
                    green= 200 # Quantity of Green
                    blue = 200 # Quantity of Blue
                elif ndvi_tuple[i] >= 0.0 and ndvi_tuple[i] < 0.3:
                    outClass = 1
                    red = 127
                    green= 255
                    blue = 212
                elif ndvi_tuple[i] >= 0.3 and ndvi_tuple[i] < 0.4:
                    outClass = 2
                    red = 0
                    green= 145
                    blue = 255
                elif ndvi_tuple[i] >= 0.4 and ndvi_tuple[i] < 0.44:
                    outClass = 3
                    red = 62
                    green= 174
                    blue = 141
                elif ndvi_tuple[i] >= 0.44 and ndvi_tuple[i] < 0.5:
                    outClass = 4
                    red = 129
                    green= 139
                    blue = 21
                elif ndvi_tuple[i] >= 0.5 and ndvi_tuple[i] < 0.6:
                    outClass = 5
                    red = 0
                    green= 139
                    blue = 0
                elif ndvi_tuple[i] >= 0.6:
                    outClass = 6
                    red = 255
                    green= 165
                    blue = 0
                else:
                    outClass = 7
                    red = 0
                    green = 0
                    blue = 0
                # Add the current pixel values to the output lines
                outputLine = struct.pack('f', outClass)
                outputLineR = struct.pack('B', red)
                outputLineG = struct.pack('B', green)
                outputLineB = struct.pack('B', blue)
            # Write the completed lines to the output images
            outDataset.GetRasterBand(1).WriteRaster(0, line, ndvi_band.XSize, 1, \
                                            outputLine, buf_xsize=ndvi_band.XSize, \
                                            buf_ysize=1, buf_type=gdal.GDT_Float32)
            outDatasetQKL.GetRasterBand(1).WriteRaster(0, line, ndvi_band.XSize, 1, \
                                            outputLineR, buf_xsize=ndvi_band.XSize, \
                                            buf_ysize=1, buf_type=gdal.GDT_Byte)
            outDatasetQKL.GetRasterBand(2).WriteRaster(0, line, ndvi_band.XSize, 1, \
                                            outputLineG, buf_xsize=ndvi_band.XSize, \
                                            buf_ysize=1, buf_type=gdal.GDT_Byte)
            outDatasetQKL.GetRasterBand(3).WriteRaster(0, line, ndvi_band.XSize, 1, \
                                            outputLineB, buf_xsize=ndvi_band.XSize, \
                                            buf_ysize=1, buf_type=gdal.GDT_Byte)
            # Delete the output lines following write
            del outputLine
            del outputLineR
            del outputLineG
            del outputLineB
        print ('Classification Completed Outputted to File')
        
    # The function from which the script runs.
    def run(self):
        # Define the input and output images
        filePath = "C:\\Users\DmytroR\Desktop\\unit9\\unit9\data\\orthol7_20423xs100999_NDVI.tif"
        outFilePathQKL = "C:\\Users\DmytroR\Desktop\\unit9\\unit9\data\\orthol7_20423xs100999_NDVI_RGB.tif"
        outFilePathSpatial = "C:\\Users\DmytroR\Desktop\\unit9\\unit9\data\\orthol7_20423xs100999_NDVI_class.tif"
        # Check the input file exists
        # Check the input file exists
        if os.path.exists(filePath):
            # Run the classify image function
            self.classifyImage(filePath, outFilePathQKL, outFilePathSpatial)
        else:
            print('The file does not exist.')

# Start the script by calling the run function.
if __name__ == '__main__':
    obj = GDALRuleClassifier()
    obj.run()